package de.audioattack.yacy31c3search.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;

/**
 * Convenience methods for handling settings.
 *
 * @author Marc Nause <marc.nause@gmx.de>
 */
public final class Settings {

    public static final String KEY_USE_HTTPS = Settings.class.getName() + "_use_https";
    public static final String KEY_HOST = Settings.class.getName() + "_host";
    public static final String DEFAULT_HOST = "31c3.yacy.net";
    public static final String SETTINGS_FILE_NAME = "settings";

    /**
     * Private constructor to avoid instatiation of static helper class.
     */
    private Settings() {
    }

    public static boolean store(final Context context, final String key, final String value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putString(key, Base64.encodeToString(value.getBytes(), Base64.NO_WRAP));
        return editor.commit();
    }

    public static boolean store(final Context context, final String key, final boolean value) {
        final SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static String load(final Context context, final String key, final String dflt) {
        final SharedPreferences sharedPreferences = getSharedPreferences(context);
        final String base64Value = sharedPreferences.getString(key, "");

        final String value;
        if (TextUtils.isEmpty(base64Value)) {
            value = dflt;
        } else {
            value = new String(Base64.decode(base64Value, Base64.NO_WRAP));
        }

        return value;
    }

    public static boolean load(final Context context, final String key, final boolean dflt) {
        return getSharedPreferences(context).getBoolean(key, dflt);
    }

    private static SharedPreferences getSharedPreferences(final Context context) {
        return context.getSharedPreferences(SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getEditor(final Context context) {
        return getSharedPreferences(context).edit();
    }
}
