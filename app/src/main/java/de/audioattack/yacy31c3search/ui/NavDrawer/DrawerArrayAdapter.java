package de.audioattack.yacy31c3search.ui.NavDrawer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * @author Marc Nause <marc.nause@gmx.de>
 */
public class DrawerArrayAdapter extends ArrayAdapter<DrawerItem> {

    private final LayoutInflater inflater;

    private final int layoutId;
    private final int textViewId;
    private final DrawerItem[] drawerItems;

    public DrawerArrayAdapter(final Context context, final int layoutId, final int textViewId, final DrawerItem[] drawerItems) {
        super(context, layoutId, textViewId, drawerItems);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.drawerItems = drawerItems;
        this.layoutId = layoutId;
        this.textViewId = textViewId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        TextView text;


        if (convertView == null) {
            view = inflater.inflate(layoutId, parent, false);
        } else {
            view = convertView;
        }

        text = (TextView) view.findViewById(textViewId);
        text.setText(drawerItems[position].text);
        text.setCompoundDrawablesWithIntrinsicBounds(drawerItems[position].icon, 0, 0, 0);

        return view;
    }

}
