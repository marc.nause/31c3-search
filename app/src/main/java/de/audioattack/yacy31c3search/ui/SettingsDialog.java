/*
 * Copyright 2014 Marc Nause <marc.nause@gmx.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see  http:// www.gnu.org/licenses/.
 */
package de.audioattack.yacy31c3search.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import de.audioattack.yacy31c3search.R;
import de.audioattack.yacy31c3search.settings.Settings;

/**
 * Allows to change settings.
 *
 * @author Marc Nause <marc.nause@gmx.de>
 */
public class SettingsDialog extends DialogFragment {

    private View customView;

    public static DialogFragment newInstance() {
        return new SettingsDialog();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // using {@link getLayoutInflator(Bundle) here will cause endless recursion
        customView = LayoutInflater.from(getActivity()).inflate(R.layout.settings_dialog, null, false);

        return new android.app.AlertDialog.Builder(getActivity())
                .setTitle(R.string.action_settings)
                .setView(customView)
                .setPositiveButton(R.string.alert_dialog_ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                final TextView tvSettingsHost = (TextView) customView.findViewById(R.id.settings_host);
                                final CheckBox cbSettingsUseHttps = (CheckBox) customView.findViewById(R.id.settings_https);
                                if (tvSettingsHost != null && tvSettingsHost.getText() != null) {
                                    Settings.store(getActivity(), Settings.KEY_HOST, (tvSettingsHost.getText().toString()));
                                    Settings.store(getActivity(), Settings.KEY_USE_HTTPS, cbSettingsUseHttps.isChecked());
                                }
                            }
                        }
                )
                .setNegativeButton(R.string.alert_dialog_dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .create();
    }

    @Override
    public void onStart() {
        super.onStart();

        final TextView tvSettingsHost = (TextView) customView.findViewById(R.id.settings_host);
        tvSettingsHost.setText(Settings.load(getActivity(), Settings.KEY_HOST, Settings.DEFAULT_HOST));

        final CheckBox cbSettingsHttps = (CheckBox) customView.findViewById(R.id.settings_https);
        cbSettingsHttps.setChecked(Settings.load(getActivity(), Settings.KEY_USE_HTTPS, false));

        // TODO: find a better way to change text color in custom view
        final Dialog dialog = getDialog();
        final int textViewId = dialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
        final TextView tv = (TextView) dialog.findViewById(textViewId);
        if (tv != null) {
            tvSettingsHost.setTextColor(tv.getCurrentTextColor());
        }
    }
}
