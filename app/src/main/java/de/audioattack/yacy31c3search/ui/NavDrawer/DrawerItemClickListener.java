package de.audioattack.yacy31c3search.ui.NavDrawer;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;

import de.audioattack.yacy31c3search.R;
import de.audioattack.yacy31c3search.ui.AlertDialog;
import de.audioattack.yacy31c3search.ui.SettingsDialog;

/**
 * @author Marc Nause <marc.nause@gmx.de>
 */
public class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {

    /**
     * Used for logging.
     */
    private static final String TAG = DrawerItemClickListener.class.getSimpleName();

    private final FragmentManager fm;

    private final DrawerLayout drawerLayout;

    public DrawerItemClickListener(final FragmentActivity activity, final DrawerLayout drawerLayout) {
        fm = activity.getSupportFragmentManager();
        this.drawerLayout = drawerLayout;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 0:
                SettingsDialog.newInstance().show(fm, "settings");
                break;
            case 1:
                AlertDialog.newInstance(R.string.action_about, R.string.about_message).show(fm
                        , "about");
                break;
            default:
                Log.w(TAG, "Clicked unknown drawer item: #" + position);
                break;
        }

        drawerLayout.closeDrawer(Gravity.START);

    }
}
